package example.util;

import example.entity.City;
import example.entity.District;
import example.model.CityModel;
import example.model.DistrictModel;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.*;

public class SampleDB {
    public static void main(String[] args) {
        CityModel cityModel = new CityModel();
        DistrictModel districtModel = new DistrictModel();
        String fileName = "src\\main\\resources\\database.json";

        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(new JSONTokener(new InputStreamReader(new FileInputStream(fileName), "UTF-8")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String cityName = jsonObject.getString("city");
            String provinceName = jsonObject.getString("province");

            City city = new City();
            city.setName(cityName);

            District district = new District();
            district.setName(provinceName);

           City resultCity = cityModel.store(city);
           District resultDistrict = districtModel.store(district);
        }
    }
}
