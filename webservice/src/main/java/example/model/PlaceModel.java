package example.model;

import com.google.gson.Gson;
import example.entity.Place;
import example.entity.cusEnum.Status;
import example.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class PlaceModel {
    Transaction transaction = null;

    public Place store(Place place) {
        try (Session session = HibernateUtil.getSession()) {
            transaction = session.beginTransaction();
            session.saveOrUpdate(place);
            transaction.commit();
            return place;
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
            return place;
        }
    }

    public List<Place> getAll() {
        try (Session session = HibernateUtil.getSession()) {
            List<Place> places = session.createQuery("from Place ", Place.class).list();
            return places;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public Place getById(long id) {
        try (Session session = HibernateUtil.getSession()) {
            transaction = session.beginTransaction();
            Place place = session.get(Place.class, id);
            return place;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public Place update(Place place) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSession()) {
            transaction = session.beginTransaction();
            session.update(place);
            transaction.commit();
            return place;
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            return null;
        }
    }

    public Place delete(long id) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSession()) {
            transaction = session.beginTransaction();
            Place place = session.get(Place.class, id);
            if (place != null) {
                place.setStatus(Status.DELETED.getValue());
                session.update(place);
            }
            transaction.commit();
            return place;
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            return null;
        }
    }
}
