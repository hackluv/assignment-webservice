package example.DTO;

import example.entity.Member;
import example.util.StringUtil;

public class MemberDTO {
    private String id;
    private String name;
    private String username;
    private String password;
    private String role;
    private String createdAt;
    private String updatedAt;
    private String status;
    private String gender;

    public MemberDTO() {
    }

    public MemberDTO(Member member) {
        this.id = String.valueOf(member.getId());
        this.name = member.getName();
        this.username = member.getUsername();
        this.role = new StringUtil().convertIntRoleToString(member.getRole());
        this.createdAt = new StringUtil().convertMilToString(member.getCreatedAt());
        this.updatedAt = new StringUtil().convertMilToString(member.getUpdatedAt());
        this.status = new StringUtil().convertIntStatusToString(member.getStatus());
        this.gender = new StringUtil().convertIntGenderToString(member.getGender());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
