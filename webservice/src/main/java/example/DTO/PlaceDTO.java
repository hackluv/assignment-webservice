package example.DTO;

import example.entity.Place;
import example.util.StringUtil;

import java.util.List;

public class PlaceDTO {
    private String id;
    private String name;
    private String images;
    private String createdAt;
    private String updatedAt;
    private String status;
    private Object cityDTO;
    private Object districtDTO;
    private Object commentDTOList;
    private int countRate;
    private Object memberDTO;

    public PlaceDTO() {
    }

    public PlaceDTO(Place place, Object cityDTO, Object districtDTO, Object commentDTOList, int countRate, Object memberDTO) {
        this.id = String.valueOf(place.getId());
        this.name = place.getName();
        this.images = place.getImages();
        this.createdAt = new StringUtil().convertMilToString(place.getCreatedAt());
        this.updatedAt = new StringUtil().convertMilToString(place.getUpdatedAt());
        this.status = new StringUtil().convertIntStatusToString(place.getStatus());
        this.cityDTO = cityDTO;
        this.districtDTO = districtDTO;
        this.commentDTOList = commentDTOList;
        this.countRate = countRate;
        this.memberDTO = memberDTO;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getCityDTO() {
        return cityDTO;
    }

    public void setCityDTO(Object cityDTO) {
        this.cityDTO = cityDTO;
    }

    public Object getDistrictDTO() {
        return districtDTO;
    }

    public void setDistrictDTO(Object districtDTO) {
        this.districtDTO = districtDTO;
    }

    public Object getCommentDTOList() {
        return commentDTOList;
    }

    public void setCommentDTOList(Object commentDTOList) {
        this.commentDTOList = commentDTOList;
    }

    public int getCountRate() {
        return countRate;
    }

    public void setCountRate(int countRate) {
        this.countRate = countRate;
    }

    public Object getMemberDTO() {
        return memberDTO;
    }

    public void setMemberDTO(Object memberDTO) {
        this.memberDTO = memberDTO;
    }
}
