package example.DTO;

import example.entity.District;
import example.util.StringUtil;

public class DistrictDTO {
    private String id;
    private String name;
    private String createdAt;
    private String updatedAt;
    private String status;

    public DistrictDTO() {
    }

    public DistrictDTO(District district) {
        this.id = String.valueOf(district.getId());
        this.name = district.getName();
        this.createdAt = new StringUtil().convertMilToString(district.getCreatedAt());
        this.updatedAt = new StringUtil().convertMilToString(district.getUpdatedAt());
        this.status = new StringUtil().convertIntStatusToString(district.getStatus());;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
