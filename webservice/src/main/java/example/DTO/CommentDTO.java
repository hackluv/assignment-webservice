package example.DTO;

import example.entity.Comment;
import example.util.StringUtil;

public class CommentDTO {
    private String id;
    private String comment;
    private String createdAt;
    private String updatedAt;
    private String status;
    private Object memberDTO;

    public CommentDTO() {
    }

    public CommentDTO(Comment comment, Object memberDTO) {
        this.id = String.valueOf(comment.getId());
        this.comment = comment.getComment();
        this.createdAt = new StringUtil().convertMilToString(comment.getCreatedAt());
        this.updatedAt = new StringUtil().convertMilToString(comment.getUpdatedAt());
        this.status = new StringUtil().convertIntStatusToString(comment.getStatus());
        this.memberDTO = memberDTO;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getMemberDTO() {
        return memberDTO;
    }

    public void setMemberDTO(Object memberDTO) {
        this.memberDTO = memberDTO;
    }
}
