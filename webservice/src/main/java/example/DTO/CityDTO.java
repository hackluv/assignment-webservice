package example.DTO;

import example.entity.City;
import example.util.StringUtil;

public class CityDTO {
    private String id;
    private String name;
    private String createdAt;
    private String updatedAt;
    private String status;

    public CityDTO() {
    }

    public CityDTO(City city) {
        this.id = String.valueOf(city.getId());
        this.name = city.getName();
        this.createdAt = new StringUtil().convertMilToString(city.getCreatedAt());
        this.updatedAt = new StringUtil().convertMilToString(city.getUpdatedAt());
        this.status = new StringUtil().convertIntStatusToString(city.getStatus());;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
