package example;

import com.google.gson.Gson;
import example.DTO.*;
import example.entity.Comment;
import example.entity.Member;
import example.entity.Place;
import example.entity.Rating;
import example.model.*;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;
import java.util.ArrayList;
import java.util.List;

@WebService()
public class HelloWorld {
    private MemberModel memberModel = new MemberModel();
    private DistrictModel districtModel = new DistrictModel();
    private CityModel cityModel = new CityModel();
    private PlaceModel placeModel = new PlaceModel();
    private RatingModel ratingModel = new RatingModel();
    private CommentModel commentModel = new CommentModel();
    private Gson gson = new Gson();

    @WebMethod
    public List<PlaceDTO> getAllPlace(){
        List<Place> places = placeModel.getAll();
        List<PlaceDTO> placeDTOList = new ArrayList<>();

        for (Place place: places) {
            PlaceDTO placeDTO;
            MemberDTO memberDTO = new MemberDTO(place.getMember());
            CityDTO cityDTO = new CityDTO(place.getCity());
            DistrictDTO districtDTO = new DistrictDTO(place.getDistrict());
            List<Comment> comments = commentModel.getByPlaceId(place.getId());
            List<CommentDTO> commentDTOList = new ArrayList<>();
            List<Rating> ratingList = ratingModel.getByPlaceId(place.getId());
            int countMemberRate = ratingList.size();
            int totalValue = 0;
            for (Rating rating : ratingList) {
                totalValue += rating.getValue();
            }
            if (countMemberRate == 0 || totalValue == 0) {
                placeDTOList.add(placeDTO = new PlaceDTO(place, cityDTO, districtDTO, commentDTOList, 0 , memberDTO));
            } else {
                // dua place - cac DTO vao day
                placeDTOList.add(placeDTO = new PlaceDTO(place, cityDTO, districtDTO, commentDTOList, Math.round(totalValue / countMemberRate) , memberDTO));
            }
        }
        return placeDTOList;
    }

    @WebMethod
    public PlaceDTO getDetailPlace(String id) {
        PlaceDTO placeDTO;
        Place place = placeModel.getById(Long.parseLong(id));
        MemberDTO memberDTO = new MemberDTO(place.getMember());
        CityDTO cityDTO = new CityDTO(place.getCity());
        //district DTO - list comment ( group by place_id ) - count rating
        DistrictDTO districtDTO = new DistrictDTO(place.getDistrict());
        //list comment ( group by place_id )
        List<Comment> comments = commentModel.getByPlaceId(Long.parseLong(id));
        List<CommentDTO> commentDTOList = new ArrayList<>();
        for (Comment comment : comments) {
            CommentDTO commentDTO = new CommentDTO(comment, comment.getMember());
            commentDTOList.add(commentDTO);
        }
        //count rating
        List<Rating> ratingList = ratingModel.getByPlaceId(Long.parseLong(id));
        int countMemberRate = ratingList.size();
        int totalValue = 0;
        for (Rating rating : ratingList) {
            totalValue += rating.getValue();
        }
        if (countMemberRate == 0 || totalValue == 0) {
            placeDTO = new PlaceDTO(place, cityDTO, districtDTO, commentDTOList, 0 , memberDTO);
        } else {
            // dua place - cac DTO vao day
            placeDTO = new PlaceDTO(place, cityDTO, districtDTO, commentDTOList, Math.round(totalValue / countMemberRate) , memberDTO);
        }
        return placeDTO;
    }


    @WebMethod
    public Rating createRating(String json) {
        Rating clientRating = gson.fromJson(json, Rating.class);
        Rating rating = new Rating();
        rating.setValue(clientRating.getValue());
        Rating result = ratingModel.store(rating);
        return result;
    }


    @WebMethod
    public CommentDTO createComment(String json) {
        Comment clientComment = gson.fromJson(json, Comment.class);
        Comment comment = new Comment();
        comment.setComment(clientComment.getComment());
        Comment result = commentModel.store(comment);
        MemberDTO memberDTO = new MemberDTO(result.getMember());
        return new CommentDTO(result, memberDTO);
    }


    @WebMethod
    public MemberDTO register(String json) {
        Member clientMem = gson.fromJson(json, Member.class);
        Member member = new Member();
        member.setName(clientMem.getName());
        member.setPassword(clientMem.getPassword());
        member.setUsername(clientMem.getUsername());
        member.setRole(clientMem.getRole());
        member.setGender(clientMem.getGender());

        Member result = memberModel.register(member);
        return new MemberDTO(result);
    }

    @WebMethod
    public MemberDTO login(String usesrname, String password) {
        Member result = memberModel.login(usesrname, password);
        return new MemberDTO(result);
    }

    @WebMethod
    public String sayHelloWorldFrom(String from) {
        String result = "Hello, world, from " + from;
        System.out.println(result);
        return result;
    }

    public static void main(String[] argv) {
        Object implementor = new HelloWorld();
        String address = "http://localhost:9000/HelloWorld";
        Endpoint.publish(address, implementor);
    }
}
